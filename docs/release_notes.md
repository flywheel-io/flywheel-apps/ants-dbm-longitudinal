# Release notes

## 0.1.1_2.3.5

## __Fixes__:
* Able to get to the right template that is provided by user.
* Add `-t` command during gear run


## __Enhancement__:
* Added logging to the path of T1 images the gear used
* Append antsLongitudinalCorticalThickness.sh commandline to output text file

## 0.1.0_2.3.5

* Initial Release