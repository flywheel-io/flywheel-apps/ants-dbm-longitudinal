#!/usr/bin/env python
"""The run script"""
import logging
import sys
from pathlib import Path
from tempfile import TemporaryDirectory

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_ants_dbm_longitudinal.main import run
from fw_gear_ants_dbm_longitudinal.parser import parse_config
from fw_gear_ants_dbm_longitudinal.utils import download_files, find_matching_files

logger = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """
    Args:
        context (GearToolkitContext): flywheel Gear Toolkit Context
    """
    # Check the level of the run
    parent_cont = context.get_destination_parent()

    if parent_cont.container_type not in ["subject"]:
        logger.error(
            f"This gear must be run at the subject container not {parent_cont.container_type} level."
        )
        sys.exit(1)
    # Parse context
    input_regex, input_tags, atlases_template_dir_mapping = parse_config(gear_context)
    # Generate anatomical file mapping
    anatomical_files_mapping = find_matching_files(
        parent_cont, tags=input_tags, regex=input_regex, filetype="nifti"
    )
    data_dir = Path(TemporaryDirectory().name)
    anatomical_input_paths = download_files(anatomical_files_mapping, data_dir)
    nipype_wf_dir = context.work_dir / parent_cont.label
    nipype_wf_dir.mkdir(exist_ok=True, parents=True)

    # Run ANTS NIPYPE Workflow
    run(
        context.config,
        nipype_wf_dir,
        anatomical_input_paths,
        atlases_template_dir_mapping,
        parent_cont.label,
        context.output_dir,
    )


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        main(gear_context)
