import os
from pathlib import Path
from unittest.mock import MagicMock

import nipype.pipeline.engine as pe
import pytest
from nibabel.testing import data_path

from fw_gear_ants_dbm_longitudinal import main
from fw_gear_ants_dbm_longitudinal.interfaces import DBMLongitudinalCorticalThickness


def test_run(tmpdir, mocker):
    mock_work_dir = Path(tmpdir.mkdir("work"))
    mock_output_dir = Path(tmpdir.mkdir("output"))
    mock_atlases_template_dir = Path(tmpdir.mkdir("atlases"))
    logging_mock = mocker.patch("logging.Logger.info")

    mock_config = {
        "atlases_template": "OASIS-30_Atropos_template",
        "atropos_iteration": 5,
        "debug": True,
        "denoise_anatomical_image": 1,
        "image_dimension": 3,
        "input_regex": "t1p.*nii\\.gz",
        "input_tags": "",
        "number_of_modalities": 1,
        "rigid_alignment_to_SST": 0,
        "rigid_template_update_component": 0,
    }

    setup_dbm_nodes_mock = mocker.patch(
        "fw_gear_ants_dbm_longitudinal.main.setup_dbm_nodes"
    )
    workflow_mock = mocker.patch(
        "fw_gear_ants_dbm_longitudinal.main.config_antsL_workflow"
    )
    workflow_mock.return_value = MagicMock(
        pe.Workflow(name="antsL_workflow", base_dir=mock_work_dir)
    )

    workflow_mock.get_node.return_value._interface.cmdline = (
        "antsLongitudinalCorticalThickness.sh -x 5 -c 2 -g 1 -d 3"
    )

    main.run(
        mock_config,
        mock_work_dir,
        ["/flywheel/v0/inputs/file01.nii.gz", "/flywheel/v0/inputs/file02.nii.gz"],
        mock_atlases_template_dir,
        "subj01",
        mock_output_dir,
    )

    logging_mock.assert_called_with(
        "antsLongitudinalCorticalThickness workflow has been successfully completed..."
    )


def test_run_w_exception(tmpdir, mocker):
    mock_work_dir = Path(tmpdir.mkdir("work"))
    mock_output_dir = Path(tmpdir.mkdir("output"))
    mock_atlases_template_dir = Path(tmpdir.mkdir("atlases"))
    logging_mock = mocker.patch("logging.Logger.info")

    mock_config = {
        "atlases_template": "OASIS-30_Atropos_template",
        "atropos_iteration": 5,
        "debug": True,
        "denoise_anatomical_image": 1,
        "image_dimension": 3,
        "input_regex": "t1p.*nii\\.gz",
        "input_tags": "",
        "number_of_modalities": 1,
        "rigid_alignment_to_SST": 0,
        "rigid_template_update_component": 0,
    }

    setup_dbm_nodes_mock = mocker.patch(
        "fw_gear_ants_dbm_longitudinal.main.setup_dbm_nodes"
    )
    workflow_mock = MagicMock(pe.Workflow(name="antsL_wf"))
    workflow_mock.run.side_effect = Exception()
    workflow_mock.return_value = pe.Workflow(
        name="antsL_workflow", base_dir=mock_work_dir
    )

    with pytest.raises(SystemExit):
        main.run(
            mock_config,
            mock_work_dir,
            ["/flywheel/v0/inputs/file01.nii.gz", "/flywheel/v0/inputs/file02.nii.gz"],
            mock_atlases_template_dir,
            "subj01",
            mock_output_dir,
        )
    logging_mock.assert_called_with("Exiting....")
