import os
from pathlib import Path
from unittest.mock import MagicMock

import flywheel
import nipype.pipeline.engine as pe
import numpy as np
import pytest
from flywheel_gear_toolkit import GearToolkitContext
from nibabel.testing import data_path

from fw_gear_ants_dbm_longitudinal import utils


def test_is_file_match_raise_if_tags_not_list():
    with pytest.raises(TypeError):
        utils.FileMatcher(tags="my-tag")


def test_is_file_match_exit_if_regex_is_invalid():
    with pytest.raises(SystemExit):
        utils.FileMatcher(regex_pattern="*.")


@pytest.mark.parametrize(
    "f,res",
    [
        (flywheel.FileEntry(name="T1w.nii.gz", type="nifti", tags=["my-tag"]), True),
        (flywheel.FileEntry(name="T1w.nii.gz", type="nifti", tags=[]), False),
    ],
)
def test_is_file_match_with_tags(f, res):
    fm = utils.FileMatcher(tags=["my-tag"])
    assert fm.match(f) is res


@pytest.mark.parametrize(
    "f,res",
    [
        (flywheel.FileEntry(name="T1w.nii.gz", type="nifti", tags=["my-tag"]), True),
        (flywheel.FileEntry(name="T1w.dicom.zip", type="dicom", tags=[]), False),
    ],
)
def test_is_file_match_with_regex(f, res):
    fm = utils.FileMatcher(regex_pattern=".*.nii.gz")
    assert fm.match(f) is res


@pytest.mark.parametrize(
    "f,res",
    [
        (flywheel.FileEntry(name="T1w.nii.gz", type="nifti", tags=["my-tag"]), True),
        (flywheel.FileEntry(name="T1w.dicom.zip", type="dicom", tags=[]), False),
    ],
)
def test_is_file_match_with_regex(f, res):
    fm = utils.FileMatcher(filetype="nifti")
    assert fm.match(f) is res


def test_parse_and_zip_directory(tmp_path, mocker):
    template_dir_mock = tmp_path / "subject01" / "template_tp_01"
    file_mock = template_dir_mock / "test01.nii.gz"

    logging_mock = mocker.patch("logging.Logger.info")
    template_dir_mock.mkdir(parents=True, exist_ok=True)
    file_mock.touch()
    mock_zip_output = mocker.patch("fw_gear_ants_dbm_longitudinal.utils.zip_output")
    mock_generate_directory_listing = mocker.patch(
        "fw_gear_ants_dbm_longitudinal.utils.generate_directory_listing"
    )
    utils.parse_and_zip_directory(template_dir_mock, "output_test.txt")

    logging_mock.assert_called_with(
        f"Successfully created {template_dir_mock.name}.zip..."
    )
