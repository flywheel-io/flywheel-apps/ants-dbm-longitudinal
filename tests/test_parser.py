import os
from pathlib import Path, PosixPath
from unittest import mock
from unittest.mock import MagicMock, Mock

import flywheel
import numpy as np
import pytest
from flywheel_gear_toolkit import GearToolkitContext
from nibabel.testing import data_path

from fw_gear_ants_dbm_longitudinal import parser


def test_generate_template_file_mapping(tmp_path):

    template_dir_01 = tmp_path / "templates01"
    template_dir_02 = tmp_path / "templates02"

    template_abc = template_dir_01 / "templatesabc.nii.gz"

    template_dir_01.mkdir()
    template_abc.touch()

    template_xyz = template_dir_02 / "templateyz.nii.gz"
    not_template = template_dir_02 / "text01.txt"
    template_dir_02.mkdir()
    template_xyz.touch()
    not_template.touch()

    actual_template_file_mapping = parser.generate_template_file_mapping(Path(tmp_path))
    expected_template_file_mapping = {
        "templates01": [template_abc.as_posix()],
        "templates02": [template_xyz.as_posix()],
    }
    assert expected_template_file_mapping == actual_template_file_mapping


def test_generate_template_file_mapping_w_exception(tmp_path):

    template_dir_01 = tmp_path / "templates01"
    template_dir_02 = tmp_path / "templates02"

    template_abc = template_dir_01 / "templatesabc.nii.gz"
    template_def = template_dir_01 / "templatesdef.nii.gz"

    template_dir_01.mkdir()
    template_abc.touch()
    template_def.touch()

    template_xyz = template_dir_02 / "templateyz.nii.gz"
    not_template = template_dir_02 / "text01.txt"
    template_dir_02.mkdir()
    template_xyz.touch()
    not_template.touch()

    with pytest.raises(SystemExit):
        actual_template_file_mapping = parser.generate_template_file_mapping(
            Path(tmp_path)
        )


def test_parse_config():

    ...
