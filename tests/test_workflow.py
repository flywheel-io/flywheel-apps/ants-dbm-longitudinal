import os
from pathlib import Path
from unittest.mock import MagicMock

import nipype.pipeline.engine as pe
import pytest
from nibabel.testing import data_path
from nipype import Workflow
from nipype.interfaces.base import BaseInterface

from fw_gear_ants_dbm_longitudinal import workflow
from fw_gear_ants_dbm_longitudinal.interfaces import DBMLongitudinalCorticalThickness


def test_config_antsL_workflow(tmpdir, mocker):
    ...
    # mock_workflow = Path(tmpdir.mkdir("workflow"))
    # mock_output_dir = Path(tmpdir.mkdir("output"))
    # # dbm_mock = MagicMock(DBMLongitudinalCorticalThickness())
    # dbm_mock = mocker.patch("fw_gear_ants_dbm_longitudinal.interfaces.DBMLongitudinalCorticalThickness")
    # dbm_mock.return_value = MagicMock(BaseInterface())
    # # dbm_mock.return_value.inputs.anatomical_image.return_value = MagicMock()
    # # setup_dbm_nodes_mock = MagicMock(pe.Node(dbm_mock, name="dbm_mock"))
    # mock_node = MagicMock(pe.Node)
    # mock_node.return_value._hierarchy = MagicMock()
    # # mock_setup_dbm_node = MagicMock(spec=mock_node))
    # actual_wf = workflow.config_antsL_workflow(mock_workflow, mock_output_dir, mock_node)
    #
    # # actual_wf = workflow.config_antsL_workflow(
    # #     example_nifti_image,
    # #     example_nifti_image,
    # #     example_nifti_image,
    # #     example_nifti_image,
    # #     {},
    # #     mock_workflow,
    # #     "f2p",
    # #     "subj01",
    # #     "sess-01",
    # #     mock_output_dir,
    # # )
    # assert type(actual_wf) == type(Workflow(name="dbm_longitudinal_workflow", base_dir=mock_workflow))
