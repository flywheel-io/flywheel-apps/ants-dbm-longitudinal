# ANTs DBM Longitudinal
A Flywheel gear wrapping ANTs antsLongitudinalCorticalThickness.sh script. 
This script creates a Single Subject Template from all the anatomical images and time point based template.


## Usage  
This gear should be run on the Subject level.

### Inputs
* __registered_predefined_atlases__ (file): Registered atlases to a population based template. 
```
<working_dir_name>
└── <template_folder_name>
    ├── Priors2
    │   ├── <nifti-file-name>
    │   ├── <nifti-file-name>
    │   ├── <nifti-file-name>
    │   ├── <nifti-file-name>
    │   ├── <nifti-file-name>
    │   └── <nifti-file-name>
    ├── brain_extraction_probability_mask
    │   └── <nifti-file-name>
    ├── brain_segmentation_template
    │   └── <nifti-file-name>
    ├── extraction_registration_mask
    │   └── <nifti-file-name>
    └── t1_registration_template
        └── <nifti-file-name>
```
As shown above, each directory should be labeled with one of the following names:
- Priors2
- brain_extraction_probability_mask
- brain_segmentation_template
- extraction_registration_mask
- t1_registration_template

Within each directory, it should contain only *ONE* template/mask image, except for the `Priors2` directory. 

_Note_: If registered predefined atlases template is not provided, the gear will use the default atlases template which can be configured in the gear config (Default: OASIS-30_Atropos_template). 
_Note_: If registered predefined atlases template is not provided, the gear will use the default atlases template which can be configured in the gear config (Default: OASIS-30_Atropos_template). 

### Configuration
* __debug__ (boolean, default: False): Include debug statements in output.
* __image_dimension__ (int, default: 3): 2 or 3 (for 2- or 3-dimensional image)
* __atropos_iteration__ (int, default: 5): Number of iterations within Atropos.
* __denoise_anatomical_image__ (int, default: 1): Denoise anatomical images.
* __number_of_modalities__ (int, default: 1): Number of modalities used to construct the template:  For example,if one wanted to use multiple modalities consisting of T1, T2, and FA components, it will be 3 modalities.
* __rigid_template_update_component__ (int, default: 0): Update the single-subject template with the full affine transform (default 0).If 1, the rigid component of the affine transform will be used to update the template. Using the rigid component is desireable to reduce bias, but variations in the origin or head position across time points can cause the template head to drift out of the field of view.
* __rigid_alignment_to_SST__ (int, default: 0): If 1, register anatomical images to the single-subject template before processing with antsCorticalThickness. This potentially reduces bias caused by variable orientation and voxel spacing 
* __input_regex__ (str, default: ".*nii\\.gz"): Regular expression that matches files to be used as anatomical image inputs. (Default '.*nii\\.gz'). https://en.wikipedia.org/wiki/Regular_expression.
* __input_tags__ (str, default: ""): Tag(s) that matches files to be used as anatomical image inputs. When multiple tags are specified, they must be comma separated (e.g. anatomical,ANTsLongitudinal) 
* __atlases_template__ (str, default: "OASIS-30_Atropos_template"): If predefined atlases are not provided as an input, the gear will use this as the default predefined atlas template.


## Contributing

For more information about how to get started contributing to that gear, 
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
